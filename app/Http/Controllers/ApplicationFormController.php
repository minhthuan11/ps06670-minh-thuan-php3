<?php

namespace App\Http\Controllers;

use App\Rules\IdNumberRule;
use App\Rules\RequiredRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\ApplicationFormController;


class ApplicationFormController extends Controller
{
    public function view(){
        return view('productCategory.applicationForm');
    }
    public function store(Request $request){
        $validatedData = $request->validate([
            'username' =>[new RequiredRule],
            'password' => [new RequiredRule],
            'email' => [new RequiredRule],
            'phone' => 'starts_with:0|digits:10',
            'phone' => [new RequiredRule],
            'dob' => [new RequiredRule],
            'idnumber' => [new RequiredRule],
            'idnumber' => [new IdNumberRule]
        ],[
            'username.required'=>'xxxxxx',
            
        ]);

        $name = $request -> post('name');
        $phone = $request -> post('phone');
        $idnumber = $request -> post('idnumber');
        var_dump($name);
        var_dump($phone);
        var_dump($idnumber);
    }
}
