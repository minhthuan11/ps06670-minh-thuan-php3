<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function view(){
        $product_name = DB::table('Products')->pluck('display_name');
        return view('test.test1',['product_name' => $product_name]);
    }
    public function store(Request $request){

    }
}
