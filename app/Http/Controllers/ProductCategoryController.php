<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\ProductCategory;

class ProductCategoryController extends Controller
{
    public function create(){
        $categories = DB::table('Product_categories')->get();
        return view('productcategory.create',['categories' => $categories]);
    }

    public function store(Request $request){
        $name = $request -> post('name');
        $display_name = $request -> post('display_name');
        $product_category = new ProductCategory;
        $product_category -> product_category_name = $name;
        $product_category -> product_category_display_name = $display_name;
        $product_category -> save();
        return 'Adding Completed';
    }
}
