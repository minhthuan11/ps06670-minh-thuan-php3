<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'Product_categories';
    protected $primaryKey = 'product_category_id';
    public $timestamps = false;
}
