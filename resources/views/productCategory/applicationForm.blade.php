<h2>Register Form</h2>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{Form::open(array('url' => '/applicationform', 'method' => 'post'))}}
    <table>
        <tr>
            <td>Username</td>
            <td>{{Form::text('name')}}</td>
        </tr>

        <tr>
            <td>Password</td>
            <td>{{Form::Password('password')}}</td>
        </tr>

        <tr>
            <td>Email</td>
            <td>{{Form::email('email')}}</td>
        </tr>

        <tr>
            <td>Phone Number</td>
            <td>{{Form::text('phone')}}</td>
        </tr>

        <tr>
            <td>Date of Birth</td>
            <td>{{ Form::date('dob', date('d/m/Y')) }} </td>
        </tr>


        <tr>
            <td>ID Number:</td>
            <td>{{Form::text('idnumber')}}</td>
        </tr>
        <tr>
            <td colspan="2" align="center">{{Form::submit('Submit')}}</td>
        </tr>
    </table>
{{Form::close()}}
