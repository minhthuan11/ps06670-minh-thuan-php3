@extends('layout.master')

@section('title','Create Product Category')
    
@section('maincontent')
{{Form::open(array('url' => '/productcategory', 'method' => 'post'))}}
    <table>
        <tr>
            <td>Name</td>
            <td>{{Form::text('name')}}</td>
        </tr>

        <tr>
            <td>Display Name:</td>
            <td>{{Form::text('display_name')}}</td>
        </tr>

        <tr>
            <td>Drop down:</td>
            <td>
                <select name="product" id="">
                    <?php 
                        foreach ($categories as $category) {
                            echo '<option value='.$category->product_category_id.' >'.$category->product_category_display_name.'</option>';
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td align="center">{{Form::submit('Submit')}}</td>
        </tr>
    </table>
{{Form::close()}}

@endsection
