<html>
    <head>
        <title>Product - @yield('title')</title>
    </head>

    <body>
        <header>
            <h1>this is header</h1>
        </header>

        <nav>
            <h2>This is nav</h2>
        </nav>
        
        <div class="container">
            @section('maincontent')
            <h3>This is main content</h3>
            @show
        </div>

        <aside>
            <h3>This is aside</h3>
        </aside>

        <footer>
            <h2>This is footer</h2>
        </footer>
    </body>
</html>