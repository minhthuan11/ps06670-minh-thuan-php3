<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test1',function(){
    return view('test.test1');
});

Route::get('productcategory/create','ProductCategoryController@create');
Route::post('productcategory','ProductCategoryController@store');
Route::get('applicationform/create','ApplicationFormController@view');
Route::post('applicationform','ApplicationFormController@store');
Route::get('test/create','TestController@view');
Route::post('test','TestController@store');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('user','UserController');
Route::get('user','UserController@index');
Route::get('user.create','UserController@create');
Route::post('user.store','UserController@store');
Route::get('user.{user}','UserController@show');
Route::get('user.{user}.edit','UserController@edit');
Route::put('user.{user}','UserController@update');
Route::delete('user.{user}','UserController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
